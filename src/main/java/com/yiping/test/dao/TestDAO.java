package com.yiping.test.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TestDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Map<String, Object>> findAll() {
		System.out.println("findAll");
//		List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from cn_user");
		List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from cities");
		System.out.println(list);
		return list;
	}
}
