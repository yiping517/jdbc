package com.yiping.test.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yiping.test.dao.TestDAO;

//透過 @RequestMapping 指定從/會被對應到此hello()方法
@RestController
public class TestController {
	
		
		@Autowired
		TestDAO dao;
	    
		@RequestMapping("/")
	    public String test(){
	    	List<Map<String,Object>> list = dao.findAll();
	        return "成功";
	    }
}
